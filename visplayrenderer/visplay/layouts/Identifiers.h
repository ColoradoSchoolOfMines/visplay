/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

 
#ifndef IDENTIFIERS_H
#define IDENTIFIERS_H

namespace visplay::layouts
{

enum class Identifiers {
    DefaultLayout = 0,
    FullscreenMPV = 1,
    WebSplit = 2,
    BBWLayoutText = 3,
    BBWLayoutVideo = 4
};

}

#endif // IDENTIFIERS_H
