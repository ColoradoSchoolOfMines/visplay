/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef VISPLAYWINDOW_H
#define VISPLAYWINDOW_H

#include "mpvwidget.h"

#include "visplay/layouts/BaseLayout.h"

#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QDialog>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QtGui/QOpenGLContext>
#include <QPointer>

#undef DELETE
#include <qhttpengine/socket.h>

#include <mpv/client.h>


namespace visplay {

class Window : public QObject
{
    Q_OBJECT

    public:
        Window(int argc, char *argv[]);
        ~Window();

        int argcc = 1;
        QString status = "running";
        char *argvv[1] = {(char*)"test"};

        QPointer<QMainWindow>           main_win;
        QPointer<QWidget>               win;
        QPointer<MpvWidget>             mpv_widget;
        layouts::BaseLayout*            front_buffer;
        layouts::BaseLayout*            back_buffer;

        int                         back_buffer_layout_number;
        bool                        playback_idle;

    public Q_SLOTS:
        // http endpoints
        void open_media(QHttpEngine::Socket *httpSocket);
        void get_idle_status(QHttpEngine::Socket *httpSocket);
        void get_property(QHttpEngine::Socket *httpSocket);
        void swap_buffers(QHttpEngine::Socket *httpSocket);
        void ping(QHttpEngine::Socket *httpSocket);
        void backbuffer_change_layout(QHttpEngine::Socket *httpSocket);
        void send_text_reply(QHttpEngine::Socket *http_socket, int status_code, const QByteArray& output_string);
        void long_poll_test(QHttpEngine::Socket *httpSocket);        

        void mark_idle();
};

}
#endif // VisplayWindow_H
