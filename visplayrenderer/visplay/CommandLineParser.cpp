/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QString>
#include <QCommandLineParser>
#include <QHostAddress>
#include <QApplication>

#include "CommandLineParser.h"

namespace visplay
{

CommandLineParser::CommandLineParser(QApplication &a)
{
    application_description = "Visplay is an open-source digital signage platform.\nFor more information, go to https://coloradoschoolofmines.gitlab.io/visplay/";
    default_port = "26226"; // Must be a string for the option to work
    default_address = QHostAddress::LocalHost;

    // Adds options to the QCommandLineParser
    parser.addHelpOption();
    parser.setApplicationDescription(application_description);
    QCommandLineOption getPort(QStringList() << "p" << "port", "Passes a port to the application.", "port", default_port);
    parser.addOption(getPort);
    QCommandLineOption getHostAddress(QStringList() << "a" << "address", "Passes a host address to the application.", "address", default_address.toString());
    parser.addOption(getHostAddress);

    // Processes the options passed through the application
    parser.process(a);

    current_address.setAddress(parser.value(getHostAddress));
    current_port = parser.value(getPort).toInt();

    // Checks for valid inputs, setting them to defaults otherwise
    if (!_addressSanityChecking(current_address)) {
        qWarning("Invalid address entered; set to default...");
        current_port = default_port.toInt();
    }
    if (!_portSanityChecking(current_port)) {
        qWarning("Invalid port entered; set to default...");
        current_address = default_address;
    }
}


bool CommandLineParser::_addressSanityChecking(QHostAddress address) {
    if (address.isNull()) 
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool CommandLineParser::_portSanityChecking(int port) {
    if (port > 65535 || port < 1024) // Ports lower than 1024 are reserved and ports cannot go higher than 65535
    {
        return false;
    }
    else 
    {
        return true;
    }
    
}

int CommandLineParser::getPort() {
    return current_port;
}

QHostAddress CommandLineParser::getAddress() {
    return current_address;
}

}
