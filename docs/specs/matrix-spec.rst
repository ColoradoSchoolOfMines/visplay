Matrix Specification
####################

Visplay uses Matrix rooms to store configuration state. There are currently three types of
rooms:

List
  For storing sets where order matters such as playlists.
Set
  For storing sets where order doesn't matter such as asset registries.
Dict
  For storing key-value pairs such as asset specifications.

A Matrix room is considered a Visplay room if there exists exactly one message
in the room of type ``edu.mines.acm.visplay.room_type`` (see below). The
``edu.mines.acm.visplay.room_type`` message specifies how the following
``edu.mines.acm.visplay.add`` and ``edu.mines.acm.visplay.remove`` events are interpreted.

Due to the specific nature of our application, there are many conditions which
can cause a room to be an invalid. In these cases, the room can just be ignored.
In the future we may increase the resiliency of the room infrastructure to
handle more invalid cases, but for now, invalid rooms are not to be considered
Visplay rooms.

The following events define the events to send rooms to create and modify the data they represent.
This spec uses the same notation for types of room events as the Matrix specification.
https://matrix.org/docs/spec/client_server/r0.6.0#types-of-room-events

Room Creation Events
====================

The following event is the general event to mark a room as a Visplay room

``edu.mines.acm.visplay.room_type``
-----------------------------------

*State Event*

    ``state_key``: A zero-length string

This event is used to decide what data type this room represents. This should be
used to determine how to interpret the following ``edu.mines.acm.visplay.add``
events.

If multiple ``edu.mines.acm.visplay.room_type`` events exist in a room, that
room is not a valid Visplay room.

=============== ============ =======================================================
**Content Key** **Type**     **Description**

--------------- ------------ -------------------------------------------------------

``type``        ``RoomType`` **Required.** The data type which this room represents.
=============== ============ =======================================================

``RoomType`` (enum)
^^^^^^^^^^^^^^^^^^^

This enum represents the possible data types which a room can represent. Where
used in a room event, this enum will be represented as a string.

+---------------------------------------+
| Enum Values                           |
+---------------------------------------+
| ``edu.mines.acm.visplay.set``         |
+---------------------------------------+
| ``edu.mines.acm.visplay.list``        |
+---------------------------------------+
| ``edu.mines.acm.visplay.dict``        |
+---------------------------------------+

**Example:**

.. code:: json

  {
    "content": {
      "type": "edu.mines.acm.visplay.set"
    },
    "event_id": "$143273582443PhrSn:example.org",
    "origin_server_ts": 1432735824653,
    "room_id": "!jEsUZKDJdhlrceRyVU:example.org",
    "sender": "@example:example.org",
    "state_key": "",
    "type": "edu.mines.acm.visplay.room_type",
    "unsigned": {
      "age": 1234
    }
  }


Dictionary Rooms
================

The following events define how to modify the data represented by dictionary-type rooms.
Dictionary rooms rely on the matrix state resolution algorithm applied to state events

``edu.mines.acm.visplay.dict.update``
-------------------------------------

*State Event*

    ``state_key``: The key in the dictionary to add or modify

This event represents the addition of or a modification to a key-value pair in the room.
To remove a key from the room, set it's value to a zero-length string.

=============== ============ ====================================================
**Content Key** **Type**     **Description**

--------------- ------------ ----------------------------------------------------

``value``       ``string``   The value to associate with the ``state_key``
=============== ============ ====================================================

**Example:**

.. code:: json

    {
        "content": {
            "value": "my value"
        },
        "event_id": "$143273582443PhrSn:example.org",
        "origin_server_ts": 1432735824653,
        "room_id": "!jEsUZKDJdhlrceRyVU:example.org",
        "sender": "@example:example.org",
        "state_key": "my_key",
        "type": "edu.mines.acm.visplay.dict.update",
        "unsigned": {
            "age": 1234
        }
    }

Set Rooms
=========

The following events define how to modify the data represented by set-type rooms.
Set rooms rely on the matrix state resolution algorithm applied to state events

``edu.mines.acm.visplay.set.update``
------------------------------------

*State Event*

    ``state_key``: The element from the set to add or remove

This event represent the addition or removal to an item in the set encoded by the room.

=============== ============ ====================================================
**Content Key** **Type**     **Description**

--------------- ------------ ----------------------------------------------------

``value``       ``boolean``   ``true`` to insert the elemnt, ``false`` to remove
=============== ============ ====================================================

**Example:**

.. code:: json

    {
        "content": {
            "value": false
        },
        "event_id": "$143273582443PhrSn:example.org",
        "origin_server_ts": 1432735824653,
        "room_id": "!jEsUZKDJdhlrceRyVU:example.org",
        "sender": "@example:example.org",
        "state_key": "my_key",
        "type": "edu.mines.acm.visplay.set.update",
        "unsigned": {
            "age": 1234
        }
    }

List Rooms
==========

The following events define how to modify the data represent by list-type rooms.
List rooms use message events and Logoot/LSEQ to implement an efficient sequence CRDT.
Specifically, list rooms will be implemented using the algorithms described in the following papers:
 * `Logoot <https://hal.inria.fr/inria-00432368/document>`_
 * `LSEQ <https://hal.archives-ouvertes.fr/file/index/docid/921633/filename/fp025-nedelec.pdf>`_
 * `h-LSEQ <https://hal.archives-ouvertes.fr/file/index/docid/921655/filename/paper5.pdf>`_

``LogootID`` (list-like)
------------------------
This special type encodes a unique identifer for an element in the list. From this id, a total order
for the elements can be deduced

It is structured as follows:

.. code:: javascript

    [[num1, siteid1], [num2, siteid2], ...]

Where...
 * ``num{i}`` is an integer that identifies the location of the element.
 * ``siteid{i}`` is the full matrix username, including the homeserver domain, of the user responsible for adding the given path.

``edu.mines.visplay.list.insert``
---------------------------------

*Message Event*

This event representation the insertion of an element into the list

=============== ============ ========================================================================
**Content Key** **Type**     **Description**

--------------- ------------ ------------------------------------------------------------------------

``id``          ``LogootID`` See description above. ID to associate with element to insert
``clk``         ``int``      Logical clock maintained by the client performing the insertion
``value``       ``any``      The value to insert into the list at the position represented by ``id``
=============== ============ ========================================================================

**Example:**

.. code:: json

    {
        "content": {
            "id": [[1, "@example:example.org"], [5, "@example2:example.org"]],
            "clk": 123,
            "value": 42
        },
        "event_id": "$143273582443PhrSn:example.org",
        "origin_server_ts": 1432735824653,
        "room_id": "!jEsUZKDJdhlrceRyVU:example.org",
        "sender": "@example:example.org",
        "type": "edu.mines.acm.visplay.list.insert",
        "unsigned": {
            "age": 1234
        }
    }

``edu.mines.visplay.list.remove``
---------------------------------

*Message Event*

This event represents the removal of an element from the list. NOTE: if the ``id`` does not exist yet,
this event will be ignored until an insertion event with the provided ``id`` is observed

=============== ============ ========================================================================
**Content Key** **Type**     **Description**

--------------- ------------ ------------------------------------------------------------------------

``id``          ``LogootID`` See description above. ID of the element to remove from the list
``clk``         ``int``      Logical clock maintained by the client performing the removal
=============== ============ ========================================================================

**Example:**

.. code:: json

    {
        "content": {
            "id": [[1, "@example:example.org"], [5, "@example2:example.org"]],
            "clk": 456,
        },
        "event_id": "$143273582443PhrSn:example.org",
        "origin_server_ts": 1432735824653,
        "room_id": "!jEsUZKDJdhlrceRyVU:example.org",
        "sender": "@example:example.org",
        "type": "edu.mines.acm.visplay.list.remove",
        "unsigned": {
            "age": 1234
        }
    }

Examples
========

Only the ``type``, ``content``, and ``state_key`` of each of the events is shown for brevity.

Set Room
--------

.. code:: javascript

  // This is the first event that we will send. Its presence makes this room a
  // Visplay room representing a set type
  Event A: {
    "type": "edu.mines.acm.visplay.room_type",
    "content": { "type": "edu.mines.acm.visplay.set" }
  }
  Representation: {}

  // Add an element
  Event B: {
    "type": "edu.mines.acm.visplay.set.update",
    "state_key": "!ohea:matrix.org",
    "content": { "value": true }
  }
  Representation: {"!ohea:matrix.org"}

  // Add another element
  Event C: {
    "type": "edu.mines.acm.visplay.set.update",
    "state_key": "!qfuy:matrix.org",
    "content": { "value": true }
  }
  Representation: {"!ohea:matrix.org", "!qfuy:matrix.org"}

  // Remove the element that was added by event B
  Event D: {
    "type": "edu.mines.acm.visplay.set.update",
    "state_key": "!ohea.matrix.org",
    "content": { "value": false }
  }
  Representation: {"!qfuy:matrix.org"}

Dict Room
---------

.. code:: javascript

  // This is the first event that we will send. Its presence makes this room a
  // Visplay room representing a dict type
  Event A: {
    "type": "edu.mines.acm.visplay.room_type",
    "content": { "type": "edu.mines.acm.visplay.dict" }
  }
  Representation: {}

  // Add an element
  Event B: {
    "type": "edu.mines.acm.visplay.dict.update",
    "state_key": "youtube.url",
    "content": { "value": "https://www.youtube.com/watch?v=9bZkp7q19f0" }
  }
  Representation: {
    "youtube.url": "https://www.youtube.com/watch?v=9bZkp7q19f0"
  }

  // Add another element
  Event C: {
    "type": "edu.mines.acm.visplay.dict.update",
    "state_key": "asset.type",
    "content": { "value": "youtube" }
  }
  Representation: {
    "youtube.url": "https://www.youtube.com/watch?v=9bZkp7q19f0",
    "asset.type": "youtube"
  }

  // Add another element
  Event D: {
    "type": "edu.mines.acm.visplay.dict.update",
    "state_key": "description",
    "content": { "value": "Gangnam Style" }
  }
  Representation: {
    "youtube.url": "https://www.youtube.com/watch?v=9bZkp7q19f0",
    "asset.type": "youtube",
    "description": "Gangnam Style"
  }

  // Remove the element that was added by event B
  Event E: {
    "type": "edu.mines.acm.visplay.dict.update",
    "state_key": "youtube.url",
    "content": { "value": "" }
  }
  Representation: {
    "asset.type": "youtube",
    "description": "Gangnam Style"
  }
