GitLab Continous Integration
============================

CI/CD is how Visplay knows whether or not a certain commit
will compile correctly. Gitlab does most of the setup for us!
This is a pointer to a much better guide!

gitlab-ci.yml
-------------

`Gitlab Docs <https://docs.gitlab.com/ee/ci/quick_start/README.html>`_