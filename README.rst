Visplay
=======

See the `Visplay Documentation`_ for more detailed documentation.

.. _Visplay Documentation: https://coloradoschoolofmines.gitlab.io/visplay/

This project's goal is to make the TV's in the foyer of the buildings of Mines
display media of ACM's projects. The project uses MPV and python.

Features
--------

- Stream videos and open media files using an ``mpv`` implementation.

Getting Started
---------------

See our `Development Quickstart Guide`_ for information on how to get started
developing Visplay.

.. _Development Quickstart Guide: https://coloradoschoolofmines.gitlab.io/visplay/development-quickstart.html
