import json
import requests

from bottle import post, response, request, route, run

# The base url for the Matrix Server
matrix_url = 'http://localhost:8008/'

@route('/')
def index():
    response.content_type = 'application/json'

    return json.dumps({'a': 2, 'b': 3})

@post('/_visplay/login')
def login():
    username = request.json['username']
    password = request.json['password']

    login_payload = {
        "type": "m.login.password",
        "identifier": {
            "type": "m.id.user",
            "user": username
        },
        "password": password
    }

    login_info = requests.post(matrix_url + '_matrix/client/r0/login', json=login_payload).json()
    access_token = login_info['access_token']

    response.content_type = 'application/json'

    return json.dumps({'access_token': access_token})

@route('/_visplay/dict/new')
def new_dict():
    pass


@route('/_visplay/dict/add')
def add_dict():
    pass


@route('/_visplay/dict/remove')
def remove_dict():
    pass


if __name__ == '__main__':
    run(host = '0.0.0.0', port = 8080, debug=False, reloader=True)
