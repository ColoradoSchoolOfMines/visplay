'''
Defines the module-level object that stores data for authentication purpose
'''

import requests

credentials = None

class MatrixCredentials:
    '''
    Stores information needed for other objects to access Matrix
    '''

    def __init__(self, matrix_url, username, password):
        self.matrix_url = matrix_url
        self.username = username
        self.full_username = f'{username}:{matrix_url}'
        self.access_token = self.get_access_token(username, password)

    def get_access_token(self, username, password):
        '''
        Returns an access token provided a username and password
        '''

        login_payload = {
            "type": "m.login.password",
            "identifier": {
                "type": "m.id.user",
                "user": username
            },
            "password": password
        }

        login_info = requests.post(f'{self.matrix_url}_matrix/client/r0/login',
                json=login_payload).json()
        access_token = login_info['access_token']

        return access_token

def init_credentials(matrix_url, username, password):
    global credentials

    credentials = MatrixCredentials(matrix_url, username, password)

def credentials_required(func):
    '''
    Decorator that can be used to throw an error if credentials are not yet defined
    '''

    def check_then_run(*args, **kwargs):
        if credentials is None:
            raise Exception('credentials object not defined. Did you call init_credentials?')

        return func(*args, **kwargs)

    return check_then_run
