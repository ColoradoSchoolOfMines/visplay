from visplay_matrix.matrix_collections.credentials import init_credentials
from visplay_matrix.matrix_collections.matrix_set import MatrixSet

init_credentials('http://localhost:8008/', 'admin', 'password')

m = MatrixSet('!aDcDbzdipSIpFlHsQP:visplay.localhost')

m.add('a')
m.add('b')
m.add('c')
m.add('d')

print('a' in m) # True
print('b' in m) # True

m.remove('a')
m.remove('b')

print('a' in m) # False
print('b' in m) # False
print('c' in m) # True

for element in m:
    print(element)

# In any order:
'''
c
d
'''

print(m.as_set()) # {'c', 'd'}

m.clear()

print(len(m)) # 0 (Except rate limits may prevent all items from being removed)
