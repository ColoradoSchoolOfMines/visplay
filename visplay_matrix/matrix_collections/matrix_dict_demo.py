from visplay_matrix.matrix_collections.credentials import init_credentials
from visplay_matrix.matrix_collections.matrix_dict import MatrixDict

init_credentials('http://localhost:8008/', 'admin', 'password')

m = MatrixDict('!ubTKPewikeEHMZsEwk:visplay.localhost')

m['a'] = 1
m['b'] = 2
m['c'] = 3
m['d'] = 4

print(m['a']) # 1
print(m['b']) # 2

del m['c']
m.pop('d')

print('c' in m) # False
print('d' not in m) # True
print('e' not in m) # True

m['e'] = 5
m['f'] = 6

for key, value in m.items():
    print(key, value)

'''
a 1
b 2
e 5
f 6
'''

print(m.as_dict()) # {'a': 1, 'b': 2, 'e': 5, 'f' :6}

m.clear()

print(len(m)) # 0 (Except rate limits may prevent all items from being removed)
