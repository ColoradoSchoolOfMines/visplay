import requests

def make_room_type_event(room_type):
    '''
    Returns a dictionary that represents a state event to set a room as a dict, list, or set
    '''

    if room_type not in ["dict", "list", "set"]:
        raise Error(f"{room_type} is not a valid room type")

    state_event = {
        "type": "edu.mines.acm.visplay.room_type",
        "content": {
            "type": f"edu.mines.acm.visplay.{room_type}"
        }
    }

    return state_event

def create_room(credentials, room_type):
    '''
    Create a matrix room of the specific type. Returns the room_id of the created room
    '''

    state_event = make_room_type_event(room_type)

    create_room_payload = {
        "visibility": "public",
        "initial_state": [state_event]
    }

    room_info = requests.post(f'{credentials.matrix_url}_matrix/client/r0/createRoom',
            json=create_room_payload, 
            headers={'Authorization': f'Bearer {credentials.access_token}'}).json()
    room_id = room_info['room_id']

    return room_id

