import requests

from collections.abc import MutableSet

from visplay_matrix.matrix_collections import credentials
from visplay_matrix.matrix_collections.credentials import credentials_required
from visplay_matrix.matrix_collections.helpers import create_room

'''
Implentation based on this documentation:

https://docs.python.org/3/library/collections.abc.html
https://docs.python.org/3/reference/datamodel.html
'''
class MatrixSet(MutableSet):
    TYPE = "set"
    UPDATE_EVENT_TYPE = "edu.mines.acm.visplay.set.update"

    EXISTING_ELEMENT = True # Value representing that an element exists
    REMOVED_ELEMENT = False # Value representing that an element doesn't exist
    
    def __init__(self, room_id):
        self.room_id = room_id

    @classmethod
    def new(cls):
        '''
        Factory to generate a MatrixSet from a new edu.mines.acm.visplay.set
        room.
        '''
        room_id = create_room(credentials.credentials, MatrixSet.TYPE)

        return cls(room_id)

    @credentials_required
    def __contains__(self, element):
        '''
        Returns True if the set contains the provided element, returns False
        if the set does not contain the provided element.
        '''
        matrix_url = credentials.credentials.matrix_url
        room_id = self.room_id
        event_type = MatrixSet.UPDATE_EVENT_TYPE
        access_token = credentials.credentials.access_token

        request_url = f'{matrix_url}_matrix/client/r0/rooms/{room_id}/state/{event_type}/{element}'

        response = requests.get(request_url,
                headers={'Authorization': f'Bearer {access_token}'})

        if response.status_code == 404:
            return False

        value = response.json()["value"]

        if value == MatrixSet.REMOVED_ELEMENT:
            return False

        return True

    @credentials_required
    def _set_element_value(self, element, value):
        '''
        Helper method to mark the element as either EXISTING or REMOVED.
        '''
        matrix_url = credentials.credentials.matrix_url
        room_id = self.room_id
        event_type = MatrixSet.UPDATE_EVENT_TYPE
        access_token = credentials.credentials.access_token

        request_url = f'{matrix_url}_matrix/client/r0/rooms/{room_id}/state/{event_type}/{element}'
        update_dict_payload = {
            "value": value
        }

        event = requests.put(request_url, json=update_dict_payload,
                headers={'Authorization': f'Bearer {access_token}'}).json()

    @credentials_required
    def add(self, element):
        '''
        Adds the provided element to the set.
        '''
        self._set_element_value(element, self.EXISTING_ELEMENT)

    @credentials_required
    def discard(self, element):
        '''
        Removes the provided element from the set.
        '''
        self._set_element_value(element, self.REMOVED_ELEMENT)

    @credentials_required
    def __iter__(self):
        '''
        Returns an iterable over the native Python set.
        '''
        return iter(self.as_set())

    @credentials_required
    def __len__(self):
        '''
        Returns the number of elements in the set.
        '''
        return len(self.as_set())

    @credentials_required
    def as_set(self):
        '''
        Converts the [MatrixSet] to a native Python set.
        '''
        matrix_url = credentials.credentials.matrix_url
        room_id = self.room_id
        access_token = credentials.credentials.access_token

        request_url = f'{matrix_url}_matrix/client/r0/rooms/{room_id}/state'

        state_events = requests.get(request_url,
                headers={'Authorization': f'Bearer {access_token}'}).json()

        parsed = set()

        for state_event in state_events:
            if state_event['type'] == MatrixSet.UPDATE_EVENT_TYPE:
                if state_event['content']['value'] != MatrixSet.REMOVED_ELEMENT:
                    parsed.add(state_event['state_key'])

        return parsed
