import requests
from collections.abc import MutableMapping

from visplay_matrix.matrix_collections import credentials
from credentials import credentials_required
from visplay_matrix.matrix_collections.helpers import create_room

'''
Implentation based on this documentation:

https://docs.python.org/3/library/collections.abc.html
https://docs.python.org/3/reference/datamodel.html
'''

class MatrixDict(MutableMapping):
    TYPE = "dict"
    UPDATE_EVENT_TYPE = "edu.mines.acm.visplay.dict.update"
    REMOVED_KEY = "" # Value representing that a key was removed from the dictionary

    def __init__(self, room_id):
        self.room_id = room_id

    @classmethod
    def new(self):
        room_id = create_room(credentials.credentials, MatrixDict.TYPE)

        return cls(room_id)

    @credentials_required
    def __getitem__(self, key):
        '''
        Returns the value stored at the given key, or raises a KeyError if
        the provided key is not in the dictionary
        '''
        
        matrix_url = credentials.credentials.matrix_url
        room_id = self.room_id
        event_type = MatrixDict.UPDATE_EVENT_TYPE
        access_token = credentials.credentials.access_token

        request_url = f'{matrix_url}_matrix/client/r0/rooms/{room_id}/state/{event_type}/{key}'

        response = requests.get(request_url,
                headers={'Authorization': f'Bearer {access_token}'})

        if response.status_code == 404:
            raise KeyError

        value = response.json()["value"]

        if value == MatrixDict.REMOVED_KEY:
            raise KeyError

        return value

    @credentials_required
    def _set_value(self, key, value):
        '''
        Helper to perform the actual value setting. This is necessary to prevent
        insertion of empty keys by users
        '''

        matrix_url = credentials.credentials.matrix_url
        room_id = self.room_id
        event_type = MatrixDict.UPDATE_EVENT_TYPE
        access_token = credentials.credentials.access_token

        request_url = f'{matrix_url}_matrix/client/r0/rooms/{room_id}/state/{event_type}/{key}'
        update_dict_payload = {
            "value": value
        }

        event = requests.put(request_url, json=update_dict_payload,
                headers={'Authorization': f'Bearer {access_token}'}).json()

    @credentials_required
    def __setitem__(self, key, value):
        '''
        Stores the provided (key, value) pair, but prevents insertion of the value representing
        a key removed from the dictionary
        '''

        if value == MatrixDict.REMOVED_KEY:
            raise Exception('Insertion failed. The provided value represents '+
                    'the lack of a value')

        return self._set_value(key, value)

    @credentials_required
    def __delitem__(self, key):
        '''
        Removed a key from the dictionary by marking its value as removed
        '''

        self._set_value(key, MatrixDict.REMOVED_KEY)

    @credentials_required
    def __iter__(self):
        '''
        Returns an iterable, simply leverages Python dictionary iterables
        '''

        return iter(self.as_dict())

    @credentials_required
    def __len__(self):
        '''
        Returns the number of elements in the dictionary, again leveraging Python dictionaries
        '''

        return len(self.as_dict())

    @credentials_required
    def as_dict(self):
        matrix_url = credentials.credentials.matrix_url
        room_id = self.room_id
        event_type = MatrixDict.UPDATE_EVENT_TYPE
        access_token = credentials.credentials.access_token

        request_url = f'{matrix_url}_matrix/client/r0/rooms/{room_id}/state'

        state_events = requests.get(request_url,
                headers={'Authorization': f'Bearer {access_token}'}).json()

        parsed = {}

        for state_event in state_events:
            if state_event['type'] == MatrixDict.UPDATE_EVENT_TYPE:
                if state_event['content']['value'] != MatrixDict.REMOVED_KEY:
                    parsed[state_event['state_key']] = state_event['content']['value']

        return parsed

