import requests
import json

# The base url for the Matrix Server
matrix_url = 'http://localhost:8008/'

def login(username, password):
    '''
    Returns an access token provided a username and password
    '''

    login_payload = {
        "type": "m.login.password",
        "identifier": {
            "type": "m.id.user",
            "user": username
        },
        "password": password
    }

    login_info = requests.post(matrix_url + '_matrix/client/r0/login', json=login_payload).json()
    access_token = login_info['access_token']

    return access_token

def make_room_type_event(room_type):
    '''
    Returns a dictionary that represents a state event to set a room as a dict, list, or set
    '''

    if room_type not in ["dict", "list", "set"]:
        raise Error(f"{room_type} is not a valid room type")

    state_event = {
        "type": "edu.mines.acm.visplay.room_type",
        "content": {
            "type": f"edu.mines.acm.visplay.{room_type}"
        }
    }

    return state_event

def create_room(access_token, room_type):
    '''
    Create a matrix room of the specific type. Returns the room_id of the created room
    '''

    state_event = make_room_type_event(room_type)

    create_room_payload = {
        "visibility": "public",
        "initial_state": [state_event]
    }

    room_info = requests.post(matrix_url + '_matrix/client/r0/createRoom',
            json=create_room_payload, 
            headers={'Authorization': f'Bearer {access_token}'}).json()
    room_id = room_info['room_id']

    return room_id

def create_dict(access_token):
    return create_room(access_token, "dict")

def create_list(access_token):
    return create_room(access_token, "list")

def create_set(access_token):
    return create_room(access_toke, "set")

def get_room_type(access_token, room_id):
    '''
    Returns the content the room type (dict, list, or set) for a provided room id
    '''

    event_type = "edu.mines.acm.visplay.room_type"

    request_url = f'{matrix_url}_matrix/client/r0/rooms/{room_id}/state/{event_type}'

    state_info = requests.get(
        request_url,
        headers={'Authorization': f'Bearer {access_token}'}
    ).json()

    return state_info['type']

def update_dict(access_token, room_id, key, value):
    '''
    Send the state event to update the (key, value) pair in the given dictionary room
    '''

    event_type = "edu.mines.acm.visplay.dict.update"
    update_dict_payload = {
        "value": value
    }

    event_id = requests.put(matrix_url + 
            f'_matrix/client/r0/rooms/{room_id}/state/{event_type}/{key}', 
            json=update_dict_payload,
            headers={'Authorization': f'Bearer {access_token}'}).json()

    return event_id['event_id']

def parse_dict(access_token, room_id):
    '''
    Returns the data in the dictionary room as a python dictionary
    '''

    event_type = "edu.mines.acm.visplay.dict.update"
    request_url = f'{matrix_url}_matrix/client/r0/rooms/{room_id}/state'

    state_events = requests.get(request_url, 
            headers={'Authorization': f'Bearer {access_token}'}).json()

    parsed = {}

    for state_event in state_events:
        if state_event['type'] == 'edu.mines.acm.visplay.dict.update':
            parsed[state_event['state_key']] = state_event['content']['value']

    return parsed

access_token = login("admin", "password")

# Get your own room_id by running create_dict, and using the return value
room_id = "!jSYxQCYOfphxjKcDZo:visplay.localhost"

update_dict(access_token, room_id, "my_key", "val4")
update_dict(access_token, room_id, "my_key2", "val3")

print(parse_dict(access_token, room_id))
