import unittest

from itertools import permutations

from visplay_matrix.logoot.structs import Path
from visplay_matrix.logoot.logoot_list_streamer import LogootListStreamer, InsertEvent, RemoveEvent

class TestBadOrder(unittest.TestCase):
    '''
    Verifies the LogootListStreamer class can handle out-of-order events
    '''

    def assert_permutations_consistent(self, event_stream, expected_list):
        for stream in permutations(event_stream):
            test_ll = LogootListStreamer('b', 16)

            test_ll.apply_stream(stream)

            self.assertEqual(test_ll.as_list(), expected_list)

    def test_small_stream(self):
        ll = LogootListStreamer('a', 16)

        event_a = ll.insert(0, 'a')
        event_b = ll.remove(0)
        event_c = ll.insert(0, 'c')

        event_stream = [InsertEvent(*event_a), RemoveEvent(*event_b),
                InsertEvent(*event_c)]

        self.assert_permutations_consistent(event_stream, ['c'])

    def test_large_stream(self):
        ll = LogootListStreamer('a', 16)

        event_a = ll.insert(0, 'a')
        event_b = ll.insert(1, 'b')
        event_c = ll.insert(1, 'c')
        event_d = ll.remove(2)
        event_e = ll.remove(1)

        event_stream = [InsertEvent(*event_a), InsertEvent(*event_b),
                InsertEvent(*event_c), RemoveEvent(*event_d),
                RemoveEvent(*event_e)]

        self.assert_permutations_consistent(event_stream, ['a'])

class TestDuplicateInsertion(TestBadOrder):
    '''
    Verifies the LogootListStreamer class can handle duplicate insertion events
    (even when events are out-of-order)
    '''

    def test_small_stream(self):
        ll = LogootListStreamer('a', 16)

        event_a = ll.insert(0, 'a')
        event_b = ll.remove(0)
        event_c = ll.insert(0, 'c')

        event_stream = [InsertEvent(*event_a), InsertEvent(*event_a),
                RemoveEvent(*event_b), InsertEvent(*event_c)]

        self.assert_permutations_consistent(event_stream, ['c'])

    def test_large_stream(self):
        ll = LogootListStreamer('a', 16)

        event_a = ll.insert(0, 'a')
        event_b = ll.insert(1, 'b')
        event_c = ll.insert(1, 'c')
        event_d = ll.remove(2)
        event_e = ll.remove(1)

        event_stream = [InsertEvent(*event_a), InsertEvent(*event_b),
                InsertEvent(*event_b), InsertEvent(*event_c),
                RemoveEvent(*event_d), RemoveEvent(*event_e)]

        self.assert_permutations_consistent(event_stream, ['a'])

class TestDuplicateRemoval(TestBadOrder):
    '''
    Verifies the LogootListStreamer class can handle duplicate removal events
    (even when events are out-of-order)
    '''

    def test_small_stream(self):
        ll = LogootListStreamer('a', 16)

        event_a = ll.insert(0, 'a')
        event_b = ll.remove(0)
        event_c = ll.insert(0, 'c')

        event_stream = [InsertEvent(*event_a), RemoveEvent(*event_b),
                RemoveEvent(*event_b), InsertEvent(*event_c)]

        self.assert_permutations_consistent(event_stream, ['c'])

    def test_large_stream(self):
        ll = LogootListStreamer('a', 16)

        event_a = ll.insert(0, 'a')
        event_b = ll.insert(1, 'b')
        event_c = ll.insert(1, 'c')
        event_d = ll.remove(2)
        event_e = ll.remove(1)

        event_stream = [InsertEvent(*event_a),
                InsertEvent(*event_b), InsertEvent(*event_c),
                RemoveEvent(*event_d), RemoveEvent(*event_e),
                RemoveEvent(*event_e)]

        self.assert_permutations_consistent(event_stream, ['a'])

class TestBadOrderDuplicates(TestBadOrder):
    '''
    Verifies that LogootListStream class can handle out of order stream
    containing duplicate insertions AND removals
    '''

    def test_small_stream(self):
        ll = LogootListStreamer('a', 16)

        event_a = ll.insert(0, 'a')
        event_b = ll.remove(0)
        event_c = ll.insert(0, 'c')

        event_stream = [InsertEvent(*event_a), RemoveEvent(*event_b),
                RemoveEvent(*event_b), InsertEvent(*event_c),
                InsertEvent(*event_c)]

        self.assert_permutations_consistent(event_stream, ['c'])

    def test_large_stream(self):
        pass # Don't repeat the large stream test from TestBadOrder

if __name__ == '__main__':
    unittest.main()
