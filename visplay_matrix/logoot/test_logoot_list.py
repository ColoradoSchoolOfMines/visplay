import unittest
import random

from unittest.mock import patch

from visplay_matrix.logoot.logoot_list import LogootList
from visplay_matrix.logoot.structs import Path, Segment

class TestLogootListInsertion(unittest.TestCase):
    '''
    Tests the high-level insertion behavoir of the LogootList class
    '''

    def setUp(self):
        self.list_a = LogootList('a', 16)
        self.list_b = LogootList('b', 16)
        random.seed(0)  # Seed the random number generator to be able to reproduce any errors here

    def test_single_insert(self):
        self.list_a.insert(0, 15)

        self.assertEqual(self.list_a.as_list(), [15])

    def test_multiple_insert(self):
        self.list_a.insert(0, 11)
        self.list_a.insert(0, 17)
        self.list_a.insert(1, 5)

        self.assertEqual(self.list_a.as_list(), [17, 5, 11])

    def test_prevent_large_index_insert(self):
        self.assertRaises(Exception, self.list_a.insert, 1, 'a')

    def test_prevent_negative_index_insert(self):
        self.assertRaises(Exception, self.list_a.insert, -1, 'b')

    def test_single_sequential_apply_insert(self):
        insert_info = self.list_a.insert(0, 'a')
        self.list_b.apply_insert(*insert_info)

        self.assertEqual(self.list_a.as_list(), ['a'])
        self.assertEqual(self.list_a.as_list(), self.list_b.as_list())

    def test_multiple_sequential_apply_insert(self):
        insert_info_a = self.list_a.insert(0, 'a')
        self.list_b.apply_insert(*insert_info_a)

        insert_info_b = self.list_b.insert(1, 'b')
        self.list_a.apply_insert(*insert_info_b)
        
        self.assertEqual(self.list_a.as_list(), ['a', 'b'])
        self.assertEqual(self.list_a.as_list(), self.list_b.as_list())

    def test_wrong_order_apply__insert(self):
        insert_info_a = self.list_a.insert(0, 'a')
        insert_info_b = self.list_a.insert(1, 'b')

        self.list_b.apply_insert(*insert_info_b)
        self.list_b.apply_insert(*insert_info_a)
        
        self.assertEqual(self.list_a.as_list(), ['a', 'b'])
        self.assertEqual(self.list_a.as_list(), self.list_b.as_list())

    # For the simulateous tests, don't check the actual order, just that the order is consistent
    def test_simultaneous_apply_insert(self):
        # Force the insertion to happen at the same location
        self.list_a = LogootList('a', 3)
        self.list_b = LogootList('b', 3)

        insert_info_a = self.list_a.insert(0, 'a')
        insert_info_b = self.list_b.insert(0, 'b')

        self.list_a.apply_insert(*insert_info_b)
        self.list_b.apply_insert(*insert_info_a)
        
        # Don't care about the actual order determined here, only that it is consistent
        self.assertEqual(self.list_a.as_list(), self.list_b.as_list())

class TestLogootListRemoval(unittest.TestCase):
    '''
    Tests high-level removal behavoir of the LogootList class
    '''

    def setUp(self):
        self.list_a = LogootList('a', 16)
        self.list_b = LogootList('b', 16)
        random.seed(0)  # Seed the random number generator to be able to
                        # reproduce any errors here

    def test_single_remove(self):
        self.list_a.insert(0, 'a')
        self.list_a.insert(1, 'b')
        self.list_a.insert(2, 'c')

        self.list_a.remove(1)

        self.assertEqual(self.list_a.as_list(), ['a', 'c'])

    def test_multiple_remove(self):
        self.list_a.insert(0, 'a')
        self.list_a.insert(1, 'b')
        self.list_a.insert(2, 'c')
        self.list_a.insert(3, 'd')
        self.list_a.insert(4, 'e')

        self.list_a.remove(0)
        self.list_a.remove(2)

        self.assertEqual(self.list_a.as_list(), ['b', 'c', 'e'])

    def test_prevent_large_index_remove(self):
        self.list_a.insert(0, 'a')
        self.list_a.insert(1, 'b')
        self.list_a.insert(2, 'c')

        self.assertRaises(Exception, self.list_a.remove, 3)

    def test_prevent_negative_index_remove(self):
        self.assertRaises(Exception, self.list_a.remove, -1)

    def test_prevent_invalid_apply_remove(self):
        # Verifies that removing an i.d. that doesn't exist throws an exception
        # This is a valid use case if a remove happens to arrive before that element's insert
        # Whatever object streams information to build the list should use try/catch and a queue
        # To make sure everything gets applied

        self.assertRaises(Exception, self.list_a.apply_remove, Path([(1, 'a')]), 0)

    def test_remove_to_empty(self):
        self.list_a.insert(0, 'a')
        self.list_a.insert(1, 'b')
        self.list_a.insert(2, 'c')

        self.list_a.remove(1)
        self.list_a.remove(0)
        self.list_a.remove(0)

        self.assertEqual(self.list_a.as_list(), [])

    def test_single_sequential_apply_remove(self):
        info_a = self.list_a.insert(0, 'a')
        info_b = self.list_a.insert(1, 'b')

        self.list_b.apply_insert(*info_a)
        self.list_b.apply_insert(*info_b)

        info_c = self.list_a.remove(1)

        self.list_b.apply_remove(*info_c)

        self.assertEqual(self.list_a.as_list(), ['a'])
        self.assertEqual(self.list_a.as_list(), self.list_b.as_list())

    def test_multiple_sequential_apply_remove(self):
        info_a = self.list_a.insert(0, 'a')
        info_b = self.list_a.insert(1, 'b')
        info_c = self.list_a.insert(2, 'c')

        self.list_b.apply_insert(*info_a)
        self.list_b.apply_insert(*info_b)
        self.list_b.apply_insert(*info_c)

        info_d = self.list_b.remove(2)

        self.list_a.apply_remove(*info_d)

        info_e = self.list_a.remove(0)

        self.list_b.apply_remove(*info_e)

        self.assertEqual(self.list_a.as_list(), ['b'])
        self.assertEqual(self.list_a.as_list(), self.list_b.as_list())

    def test_simultaneous_apply_remove(self):
        info_a = self.list_a.insert(0, 'a')
        info_b = self.list_a.insert(1, 'b')
        info_c = self.list_a.insert(2, 'c')

        self.list_b.apply_insert(*info_a)
        self.list_b.apply_insert(*info_b)
        self.list_b.apply_insert(*info_c)

        info_d = self.list_a.remove(0)
        info_e = self.list_b.remove(1)

        self.list_a.apply_remove(*info_e)
        self.list_b.apply_remove(*info_d)

        self.assertEqual(self.list_a.as_list(), ['c'])
        self.assertEqual(self.list_a.as_list(), self.list_b.as_list())

    def test_wrong_order_apply_remove(self):
        info_a = self.list_a.insert(0, 'a')
        info_b = self.list_a.insert(1, 'b')
        info_c = self.list_a.insert(2, 'c')

        self.list_b.apply_insert(*info_a)
        self.list_b.apply_insert(*info_b)
        self.list_b.apply_insert(*info_c)

        info_d = self.list_a.remove(1)
        info_e = self.list_a.remove(1)

        self.list_b.apply_remove(*info_e)
        self.list_b.apply_remove(*info_d)

        self.assertEqual(self.list_a.as_list(), ['a'])
        self.assertEqual(self.list_a.as_list(), self.list_b.as_list())

if __name__ == '__main__':
    unittest.main()
