'''
This files defines some of the fundamental data structures that underly the Logoot alg
(Path and Segment)
'''

import math
from functools import total_ordering

# total_ordering will overload all the comparision operates from definitions of just == and <
@total_ordering
class Path:
    '''
    Represents a sequence of segments, to form the full path to an element in the "tree"
    that the collection of LogootIDs forms
    '''

    def __init__(self, segments):
        '''
        segments: list of Segment instances
        '''

        self.segments = segments

    def __eq__(self, other):
        if len(self) != len(other):
            return False

        for i in range(len(self)):
            if self.segments[i] != other.segments[i]:
                return False

        return True

    def __lt__(self, other):
        '''
        Allows paths to be compared with the "<" operator
        '''

        for i in range(min(len(self), len(other))):
            if self.segments[i] < other.segments[i]:
                return True

            elif self.segments[i] > other.segments[i]:
                return False

        return len(self) < len(other)

    def __len__(self):
        return len(self.segments)

    def __hash__(self):
        return hash(str(self.segments))

    def pop(self):
        '''
        Removes and returns the first segment in the path. Used by the Trie to deconstruct
        a path to store it in the trie recursively
        '''

        return self.segments.pop(0);

    def append(self, segment):
        '''
        Adds a new segment to the end of the path
        '''

        self.segments.append(segment)

    def get_segment(self, depth):
        '''
        Returns a the Segment instance at the provided depth in the path, or if the path does
        not contain a segment at the depth, returns a site with minimum position: (0, None)
        
        depth: integer, depth of the segment to extract
        '''

        if len(self) > depth:
            return self.segments[depth]

        else:
            return Segment(0, None)

    def is_padded(self, depth):
        '''
        Returns True if the segment at this depth is the (0, None) segment
        '''

        segment = self.get_segment(depth)

        return segment.position == 0 and segment.site is None

    def dist(self, other, tree_width=16, min_dist=2):
        '''
        Returns a (distance, depth) tuple of the distance between this and the
        provided path provided paths, at the lowest depth for which the
        distance is at least min_dist (so that min_dist-1 paths could be
        inserted between)

        path_a: Path instance, comes before path_b
        path_b: Path instance, comes after path_a 
        tree_width: maximum number of unique identifiers that can go in each row
        min_dist: minimum distance desired between the paths, integer >= 2
        ''' 

        distance = 0
        depth = 0

        while True:
            segment_a = self.get_segment(depth)
            segment_b = other.get_segment(depth)

            distance *= tree_width
            distance += segment_a.dist(segment_b)

            if distance < 0:
                # This happens when two segments have identical positions but
                # different sites somewhere in the paths, and the segments that
                # follow happen to have positions that make the distance
                # negative. This should be relatively rare in practice Directly
                # calculate the distance and depth of inserting by appending to
                # path a This is a simpler solution than trying to find space
                # within the depth of path_a

                extra_depth = math.ceil(math.log(min_dist, tree_width))
                final_depth = len(self) - 1 + extra_depth

                return (math.pow(tree_width, extra_depth), final_depth)

            if self.is_padded(depth) and other.is_padded(depth) and distance == 0:
                # TODO: update this comment to account for short gap between elements
                # Have "identical" paths, so add the max tree width, and keep iterating
                # This is caused by identical positions but different sites somewhere along the paths
                distance += tree_width

            if distance >= min_dist:
                return (distance, depth)

            depth += 1

@total_ordering
class Segment:
    '''
    Represents the couple (pos, site), a part of the variable-size identifier for Logoot
    '''

    def __init__(self, position, site):
        '''
        position: an integer to represent the order of this Identifier
        site: string, a unique identifer for each replica of the data structure
        '''

        # TODO: consider making this just pos
        self.position = position
        self.site = site

    def __eq__(self, other):
        '''
        Supports ordering paths
        '''

        return self.position == other.position and self.site == other.site

    def __lt__(self, other):
        '''
        Supports ordering paths
        '''

        return self.position < other.position or (self.position == other.position and self.site < other.site)

    def __hash__(self):
        '''
        Uses built-in hash function for tuples to let segment be keys in a dictionary
        '''

        return hash(self.as_tuple())

    def __repr__(self):
        '''
        Returns the string-representation of a segment
        '''

        return 'Segment' + str(self.as_tuple())

    def as_tuple(self):
        '''
        Returns the (position, site) tuple represented by this Segment
        '''

        return (self.position, self.site)

    def dist(self, other):
        '''
        Returns an integer, the distance between this and the other segment.
        Note that this ignores sites, so two segments with identical positions
        but different sites will be assigned a distance of 0. This greatly
        simplifies the logic required to add to the path, at the cost of less
        efficient inserts between some segments

        other: Segment instance to compute the distance between this and the other segment
        '''

        return other.position - self.position
