'''
Tests the TrieNode class (and implicitly parts of the Trie class, including the
insert method, b/c it has a better interface for high-level operations), the
backend component of a LogootList
'''

import unittest

from visplay_matrix.logoot.trie_node import TrieNode
from visplay_matrix.logoot.trie import Trie
from visplay_matrix.logoot.structs import Path

class TestAsList(unittest.TestCase):
    '''
    Verifies that the as_list functions returns a correct representation of the list
    Also implicitly tests the node's insert function
    '''

    def test_shallow_single_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'a')]), 1, 'test')

        self.assertEqual(trie.as_list(), ['test'])

    def test_shallow_multi_path(self):
        trie = Trie()

        trie.insert(Path([(4, 'c')]), 1, 'a')
        trie.insert(Path([(5, 'a')]), 2, 'b')
        trie.insert(Path([(5, 'b')]), 3, 'c')
        trie.insert(Path([(7, 'a')]), 4, 'd')

        self.assertEqual(trie.as_list(), ['a', 'b', 'c', 'd'])

    def test_deep_single_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'a'), (2, 'b'), (7, 'c')]), 1, 'test')

        self.assertEqual(trie.as_list(), ['test'])

    def test_deep_multi_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'a'), (2, 'b'), (1, 'c')]), 1, 'a')
        trie.insert(Path([(5, 'a'), (2, 'b'), (2, 'a')]), 2, 'b')
        trie.insert(Path([(5, 'a'), (2, 'b'), (2, 'b')]), 3, 'c')
        trie.insert(Path([(5, 'a'), (3, 'a'), (1, 'a')]), 4, 'd')
        trie.insert(Path([(5, 'a'), (4, 'c'), (7, 'c')]), 5, 'e')

        self.assertEqual(trie.as_list(), ['a', 'b', 'c', 'd', 'e'])

    def test_deep_mixed_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'b'), (2, 'b')]), 1, 'a')
        trie.insert(Path([(6, 'a')]), 2, 'b')
        trie.insert(Path([(6, 'a'), (2, 'b'), (2, 'a')]), 2, 'c')
        trie.insert(Path([(6, 'a'), (3, 'c')]), 3, 'd')
        trie.insert(Path([(6, 'b')]), 3, 'e')

        self.assertEqual(trie.as_list(), ['a', 'b', 'c', 'd', 'e'])

class TestAtIndex(unittest.TestCase):
    '''
    Verfies that the at_index functions returns the element at the correct index
    '''

    def test_shallow_single_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'a')]), 1, 'test')
        expected = ['test']

        for i, item in enumerate(expected):
            self.assertEqual(trie.at_index(i).value, item)

    def test_shallow_multi_path(self):
        trie = Trie()

        trie.insert(Path([(4, 'c')]), 1, 'a')
        trie.insert(Path([(5, 'a')]), 2, 'b')
        trie.insert(Path([(5, 'b')]), 3, 'c')
        trie.insert(Path([(7, 'a')]), 4, 'd')

        expected = ['a', 'b', 'c', 'd']

        for i, item in enumerate(expected):
            self.assertEqual(trie.at_index(i).value, item)

    def test_deep_single_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'a'), (2, 'b'), (7, 'c')]), 1, 'test')

        expected = ['test']

        for i, item in enumerate(expected):
            self.assertEqual(trie.at_index(i).value, item)

    def test_deep_multi_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'a'), (2, 'b'), (1, 'c')]), 1, 'a')
        trie.insert(Path([(5, 'a'), (2, 'b'), (2, 'a')]), 2, 'b')
        trie.insert(Path([(5, 'a'), (2, 'b'), (2, 'b')]), 3, 'c')
        trie.insert(Path([(5, 'a'), (3, 'a'), (1, 'a')]), 4, 'd')
        trie.insert(Path([(5, 'a'), (4, 'c'), (7, 'c')]), 5, 'e')

        expected = ['a', 'b', 'c', 'd', 'e']

        for i, item in enumerate(expected):
            self.assertEqual(trie.at_index(i).value, item)

    def test_deep_mixed_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'b'), (2, 'b')]), 1, 'a')
        trie.insert(Path([(6, 'a')]), 2, 'b')
        trie.insert(Path([(6, 'a'), (2, 'b'), (2, 'a')]), 3, 'c')
        trie.insert(Path([(6, 'a'), (3, 'c')]), 4, 'd')
        trie.insert(Path([(6, 'b')]), 5, 'e')

        expected = ['a', 'b', 'c', 'd', 'e']

        for i, item in enumerate(expected):
            self.assertEqual(trie.at_index(i).value, item)


class TestRemove(unittest.TestCase):
    '''
    Verifies that the remove method clears out a node
    '''

    def test_integration(self):
        '''
        Checks that calling remove produces the desired side-effects:
        decrementing the sizes, and removing any unused nodes
        '''

        trie = Trie()

        trie.insert(Path([(1, 'a'), (2, 'a'), (3, 'a')]), 1, 'a')
        trie.insert(Path([(1, 'a'), (2, 'a'), (4, 'a')]), 1, 'a')

        self.assertEqual(trie.root.size, 2)
        self.assertEqual(len(trie.root.child_segments), 1)

        trie.remove(Path([(1, 'a'), (2, 'a'), (3, 'a')]))
        trie.remove(Path([(1, 'a'), (2, 'a'), (4, 'a')]))

        self.assertEqual(trie.root.size, 0)
        self.assertEqual(len(trie.root.child_segments), 0)

    def test_values_cleared(self):
        trie_node = TrieNode()

        trie_node.insert('a', 1)
        trie_node.remove()

        self.assertIsNone(trie_node.clk)
        self.assertIsNone(trie_node.value)

class TestGetByPath(unittest.TestCase):
    '''
    Verifies get_by_path returns the node at the path, or None if the path doesn't exist
    '''

    def test_shallow_path_exists(self):
        trie = Trie()
        test_path = Path([(5, 'f')])

        trie.insert(test_path, 2, 'b')

        node = trie.get_by_path(test_path)

        self.assertEqual(node.value, 'b')
        self.assertEqual(node.clk, 2)

    def test_deep_path_exists(self):
        trie = Trie()
        test_path = Path([(1, 'a'), (2, 'a'), (3, 'a')])

        trie.insert(test_path, 1, 'a')

        node = trie.get_by_path(test_path)

        self.assertEqual(node.value, 'a')
        self.assertEqual(node.clk, 1)

    def test_shallow_path_not_exist(self):
        trie = Trie()

        trie.insert(Path([(5, 'f')]), 1, 'a')

        node = trie.get_by_path(Path([(3, 'b')]))

        self.assertIsNone(node)

    def test_deep_path_not_exist(self):
        trie = Trie()

        trie.insert(Path([(1, 'a'), (2, 'a'), (3, 'a')]), 1, 'a')

        node = trie.get_by_path(Path([(1, 'a'), (3, 'b'), (5, 'c')]))

        self.assertIsNone(node)

class TestCreatePath(unittest.TestCase):
    '''
    Verifies the create_path method will create and return nodes at a given
    path, or return the node already at that path, if it exists
    '''

    def test_shallow_path_exists(self):
        trie = Trie()
        test_path = Path([(5, 'f')])

        trie.insert(test_path, 2, 'b')

        node = trie.root.create_path(test_path)

        self.assertEqual(node.value, 'b')
        self.assertEqual(node.clk, 2)

    def test_deep_path_exists(self):
        trie = Trie()
        test_path = Path([(1, 'a'), (2, 'a'), (3, 'a')])

        trie.insert(test_path, 1, 'a')

        node = trie.root.create_path(test_path)

        self.assertEqual(node.value, 'a')
        self.assertEqual(node.clk, 1)

    def test_shallow_path_not_exist(self):
        trie = Trie()

        trie.insert(Path([(5, 'f')]), 1, 'a')

        node = trie.root.create_path(Path([(3, 'b')]))

        self.assertIsNone(node.value)
        self.assertIsNone(node.clk)

    def test_deep_path_not_exist(self):
        trie = Trie()

        trie.insert(Path([(1, 'a'), (2, 'a'), (3, 'a')]), 1, 'a')

        node = trie.root.create_path(Path([(1, 'a'), (3, 'b'), (5, 'c')]))

        self.assertIsNone(node.value)
        self.assertIsNone(node.clk)

class TestInsertHelpers(unittest.TestCase):
    '''
    Verifies that the inertion helper method(s) (increment_size) work correctly
    '''

    def test_integration(self):
        '''
        Checks that calling insert produces the desired side-effects:
        incrementing the sizes tracked by the nodes
        '''

        trie = Trie()

        trie.insert(Path([(1, 'a'), (2, 'a'), (3, 'a')]), 1, 'a')
        trie.insert(Path([(1, 'a'), (2, 'a'), (4, 'a')]), 1, 'a')

        self.assertEqual(trie.root.size, 2)

    def test_increment_shallow(self):
        trie = Trie()

        trie.root.create_path(Path([(1, 'a')]))

        trie.root.child_segments[(1, 'a')].increment_size()

        self.assertEqual(trie.root.size, 1)
        self.assertEqual(trie.root.child_segments[(1, 'a')].size, 1)

    def test_increment_deep(self):
        trie = Trie()

        trie.root.create_path(Path([(1, 'a'), (2, 'a')]))

        trie.root.child_segments[(1, 'a')].child_segments[(2, 'a')].increment_size()

        self.assertEqual(trie.root.size, 1)
        self.assertEqual(trie.root.child_segments[(1, 'a')].size, 1)
        self.assertEqual(trie.root.child_segments[(1, 'a')].child_segments[(2, 'a')].size, 1)

class TestRemovalHelpers(unittest.TestCase):
    '''
    Verifies that the removal helper methods (decrement_size and cleanup_nodes) work correctly
    '''

    def test_decrement_shallow(self):
        trie = Trie()

        trie.insert(Path([(1, 'a')]), 1, 'a')

        trie.at_index(0).decrement_size()

        self.assertEqual(trie.root.size, 0)
        self.assertEqual(trie.root.child_segments[(1, 'a')].size, 0)

    def test_decrement_deep(self):
        trie = Trie()

        trie.insert(Path([(1, 'a'), (2, 'a')]), 1, 'a')

        trie.at_index(0).decrement_size()

        self.assertEqual(trie.root.size, 0)
        self.assertEqual(trie.root.child_segments[(1, 'a')].size, 0)
        self.assertEqual(trie.root.child_segments[(1, 'a')].child_segments[(2, 'a')].size, 0)

    def test_cleanup_shallow(self):
        trie = Trie()

        trie.insert(Path([(1, 'a')]), 1, 'a')

        trie.at_index(0).parent.cleanup_node((1, 'a'))

        self.assertEqual(len(trie.root.child_segments), 0)

    def test_cleanup_deep(self):
        trie = Trie()

        trie.insert(Path([(1, 'a'), (2, 'a')]), 1, 'a')

        trie.at_index(0).parent.cleanup_node((2, 'a'))

        self.assertEqual(len(trie.root.child_segments), 0)

class TestGetAbsPath(unittest.TestCase):
    '''
    Verifies that the get_abs_path function returns the correct path for a node
    '''

    def test_shallow_path(self):
        trie = Trie()

        trie.insert(Path([(1, 'a')]), 1, 'a')

        computed_path = trie.at_index(0).get_abs_path()

        self.assertEqual(computed_path, Path([(1, 'a')]))


    def test_deep_path(self):
        trie = Trie()

        trie.insert(Path([(1, 'a'), (2, 'b'), (3, 'c')]), 1, 'a')

        computed_path = trie.at_index(0).get_abs_path()

        self.assertEqual(computed_path, Path([(1, 'a'), (2, 'b'), (3, 'c')]))

if __name__ == '__main__':
    unittest.main()
