'''
This file defines the LogootList class, a CRDT sequence object that implements Logoot
'''

import random

from visplay_matrix.logoot.structs import Segment, Path
from visplay_matrix.logoot.trie import Trie
from visplay_matrix.logoot.path_builder import LogootPathBuilder

class LogootList:
    def __init__(self, site, tree_width, path_builder=LogootPathBuilder):
        '''
        site: string, unique identifer for this replica of the data structure
        tree_width: integer, maximum number of positions that can exist in each layer
        path_builder: class that generates new paths for insertions
        '''

        self.site = site
        self.tree_width = tree_width
        self.path_builder = path_builder

        self.clk = 0                    # Logical clock maintained by this replica
        self.trie = Trie()              # Stores all the information contained in the list

        self.list_repr = []             # Stores a Python list of the elements stored
        self.list_repr_len = 0          # Length of the list currently represented
        self.recompute = False          # Whether or not the representation has to be recomputed
    
        # min and max path represent the elements all other elements are inserted between
        min_path = Path([Segment(0, '')])
        max_path = Path([Segment(tree_width - 1, '')])

        self.trie.insert(min_path, 0, 'START')
        self.trie.insert(max_path, 0, 'END')

    def as_list(self):
        '''
        Returns the Python list representation of the data structure
        '''

        if self.recompute:
            # Remove the first and last elements, the min and max elements
            self.list_repr = self.trie.as_list()[1:-1]

        return self.list_repr

    def at_index(self, index):
        '''
        Retrieves the value stored at the provided index in the data structure
        '''

        return self.trie.at_index(index).value

    def get_by_path(self, path):
        '''
        Returns the node in the Trie at the provided path
        '''

        return self.trie.get_by_path(path)

    def insert(self, index, value):
        '''
        Function called by the user to insert a value into the list at a given index

        index: integer between 0 and the len of the list at which to insert the value
        value: the value to insert into the list

        Returns: (path, clk, value) tuple to call apply_insert on another replica
        '''

        if index > self.list_repr_len or index < 0:
            raise Exception(f'Cannot insert at index {index} into a list of length' +
                            f'{self.list_repr_len}')
        
        lower_path = self.trie.at_index(index).get_abs_path()
        upper_path = self.trie.at_index(index + 1).get_abs_path()

        new_path = self.alloc_single_path(lower_path, upper_path)

        clk = self.clk

        self.apply_insert(new_path, self.clk, value)

        return (new_path, clk, value)

    def remove(self, index):
        '''
        Function called by the user to remove the value at the given index
        
        index: index of the element to remove, integer between 0 and length-1

        Returns (path, clk) tuple to call apply_remove on another replica
        '''

        if index >= self.list_repr_len or index < 0:
            raise Exception(f'Cannot remove at index {index} from a list of length' +
                            f'{self.list_repr_len}')

        target_node = self.trie.at_index(index + 1)

        target_path = target_node.get_abs_path()
        target_clk = target_node.clk

        self.apply_remove(target_path, target_clk)

        return (target_path, target_clk)

    def apply_insert(self, path, clk, value):
        '''
        Function to insert values that were inserted by other replicas

        path: Path instance, defines the relative location of the item beign inserted
        clk: logical clock value of the replica when this item was inserted
        value: the value to insert into the list
        '''

        self.clk = clk + 1

        self.trie.insert(path, clk, value)

        self.list_repr_len += 1
        self.recompute = True

    # TODO: should we do something with the clk value?
    def apply_remove(self, path, clk):
        '''
        Function to remove values that were removed by other replicas

        path: Path instance, defining the item to remove from the list
        clk: logical clock value of the item to remove
        '''

        try:
            self.trie.remove(path)
        except Exception:
            raise Exception('Applying removal failed. The given path may not '
                    'exist in this replica yet')

        self.list_repr_len -= 1
        self.recompute = True

    def alloc_single_path(self, path_a, path_b):
        '''
        Returns a single path instance that falls between path_a and path_b

        path_a: Path instance, comes before path_b
        path_b: Path instance, comes after path_a
        '''

        return self.path_builder.alloc_paths(path_a, path_b, 1, self.site, self.tree_width)[0]
