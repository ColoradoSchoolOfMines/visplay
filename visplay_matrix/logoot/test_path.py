import unittest

from visplay_matrix.logoot.structs import Segment, Path

class TestComp(unittest.TestCase):
    '''
    Tests that the overlaoded comparision operators work correctly
    '''

    def setUp(self):
        segments = [Segment(0, 'a'), Segment(1, 'b'), Segment(2, 'c')]

        self.path = Path(segments)

    def test_get_segment_within_depth(self):
        pos, site = self.path.get_segment(2).as_tuple()

        self.assertEqual(pos, 2)
        self.assertEqual(site, 'c')

    def test_edge_outside_depth(self):
        pos, site = self.path.get_segment(3).as_tuple()

        self.assertEqual(pos, 0)
        self.assertIsNone(site)

    def test_shallow_diff_cmp(self):
        path_a = Path([Segment(5, 'a')])
        path_b = Path([Segment(8, 'b')])

        self.assertTrue(path_a < path_b)
        self.assertFalse(path_b < path_a)
        self.assertNotEqual(path_a, path_b)

    def test_deep_diff_cmp(self):
        path_a = Path([Segment(2, 'a'), Segment(7, 'a')])
        path_b = Path([Segment(2, 'a'), Segment(12, 'b')])

        self.assertTrue(path_a < path_b)
        self.assertFalse(path_b < path_a)
        self.assertNotEqual(path_a, path_b)

    def test_distance_path_cmp(self):
        path_a = Path([Segment(2, ''), Segment(8, ''), Segment(10, '')])
        path_b = Path([Segment(7, ''), Segment(5, ''), Segment(11, '')])

        self.assertTrue(path_a < path_b)
        self.assertFalse(path_b < path_a)
        self.assertNotEqual(path_a, path_b)

    def test_shallow_site_diff_cmp(self):
        path_a = Path([Segment(2, 'a')])
        path_b = Path([Segment(2, 'b')])

        self.assertTrue(path_a < path_b)
        self.assertFalse(path_b < path_a)
        self.assertNotEqual(path_a, path_b)

    def test_deep_site_diff_cmp(self):
        path_a = Path([Segment(2, 'a'), Segment(5, 'a')])
        path_b = Path([Segment(2, 'a'), Segment(5, 'b')])

        self.assertTrue(path_a < path_b)
        self.assertFalse(path_b < path_a)
        self.assertNotEqual(path_a, path_b)

    def test_length_diff_cmp(self):
        path_a = Path([Segment(2, 'a'), Segment(5, 'a')])
        path_b = Path([Segment(2, 'a'), Segment(5, 'a'), Segment(1, 'a')])

        self.assertTrue(path_a < path_b)
        self.assertFalse(path_b < path_a)
        self.assertNotEqual(path_a, path_b)

    def test_shallow_equal_cmp(self):
        path_a = Path([Segment(5, 'a')])
        path_b = Path([Segment(5, 'a')])

        self.assertFalse(path_a < path_b)
        self.assertFalse(path_b < path_a)
        self.assertEqual(path_a, path_b)

    def test_deep_equal_cmp(self):
        path_a = Path([Segment(5, 'a'), Segment(7, 'b')])
        path_b = Path([Segment(5, 'a'), Segment(7, 'b')])

        self.assertFalse(path_a < path_b)
        self.assertFalse(path_b < path_a)
        self.assertEqual(path_a, path_b)

class TestNormalDist(unittest.TestCase):
    '''
    Tests that the distance between two paths is calculated correctly 
    when paths can be ordered by position alone
    '''

    def test_shallow_gap(self):
        path_a = Path([Segment(0, '')])
        path_b = Path([Segment(32, '')])

        distance, depth = path_a.dist(path_b, tree_width=64, min_dist=2)

        self.assertEqual(distance, 32)
        self.assertEqual(depth, 0)

    def test_shallow_no_gap(self):
        path_a = Path([Segment(16, '')])
        path_b = Path([Segment(17, '')])

        distance, depth = path_a.dist(path_b, tree_width=32, min_dist=2)

        self.assertEqual(distance, 32)
        self.assertEqual(depth, 1)

    def test_deep_gap(self):
        path_a = Path([Segment(25, ''), Segment(16, ''), Segment(0, '')])
        path_b = Path([Segment(25, ''), Segment(16, ''), Segment(64, '')])

        distance, depth = path_a.dist(path_b, tree_width=128, min_dist=2)

        self.assertEqual(distance, 64)
        self.assertEqual(depth, 2)

    def test_deep_no_gap(self):
        path_a = Path([Segment(25, ''), Segment(16, ''), Segment(9, '')])
        path_b = Path([Segment(25, ''), Segment(16, ''), Segment(10, '')])

        distance, depth = path_a.dist(path_b, tree_width=128, min_dist=2)

        self.assertEqual(distance, 128)
        self.assertEqual(depth, 3)

    def test_multi_level(self):
        path_a = Path([Segment(9, ''), Segment(10, '')])
        path_b = Path([Segment(10, ''), Segment(22, '')])

        distance, depth = path_a.dist(path_b, tree_width=32, min_dist=2)

        self.assertEqual(distance, 44)
        self.assertEqual(depth, 1)

    def test_multi_level_negative(self):
        path_a = Path([Segment(9, ''), Segment(22, '')])
        path_b = Path([Segment(10, ''), Segment(10, '')])

        distance, depth = path_a.dist(path_b, tree_width=32, min_dist=2)

        self.assertEqual(distance, 20)
        self.assertEqual(depth, 1)

    def test_short_first_path(self):
        path_a = Path([Segment(9, '')])
        path_b = Path([Segment(10, ''), Segment(37, ''), Segment(22, '')])

        distance, depth = path_a.dist(path_b, tree_width=64, min_dist=2)

        self.assertEqual(distance, 101)
        self.assertEqual(depth, 1)

    def test_long_first_path(self):
        path_a = Path([Segment(9, ''), Segment(37, ''), Segment(22, '')])
        path_b = Path([Segment(10, '')])

        distance, depth = path_a.dist(path_b, tree_width=64, min_dist=2)

        self.assertEqual(distance, 27)
        self.assertEqual(depth, 1)

    def test_long_first_path_large_dist(self):
        path_a = Path([Segment(9, ''), Segment(37, ''), Segment(22, '')])
        path_b = Path([Segment(10, '')])

        distance, depth = path_a.dist(path_b, tree_width=64, min_dist=32)

        self.assertEqual(distance, 1706)
        self.assertEqual(depth, 2)

    def test_distant_paths(self):
        path_a = Path([Segment(2, ''), Segment(8, ''), Segment(10, '')])
        path_b = Path([Segment(7, ''), Segment(5, ''), Segment(11, '')])

        distance, depth = path_a.dist(path_b, tree_width=16, min_dist=128)

        self.assertEqual(distance, 1233)
        self.assertEqual(depth, 2)

class TestSpecialPathDist(unittest.TestCase):
    '''
    Tests that the distance between two paths is caculated correctlty
    for spacial cases, where the order of the paths cannot be determined
    by position alone, which has strange implications on this calculation
    '''

    def test_shallow_identical(self):
        path_a = Path([Segment(2, 'a')])
        path_b = Path([Segment(2, 'b')])

        distance, depth = path_a.dist(path_b, tree_width=32, min_dist=2)

        self.assertEqual(distance, 32)
        self.assertEqual(depth, 1)

    def test_deep_identical(self):
        path_a = Path([Segment(2, 'a'), Segment(5, '')])
        path_b = Path([Segment(2, 'b'), Segment(5, '')])

        distance, depth = path_a.dist(path_b, tree_width=32, min_dist=2)

        self.assertEqual(distance, 32)
        self.assertEqual(depth, 2)

    def test_short_negative(self):
        path_a = Path([Segment(2, 'a'), Segment(7, '')])
        path_b = Path([Segment(2, 'b'), Segment(2, '')])

        distance, depth = path_a.dist(path_b, tree_width=64, min_dist=2)

        self.assertEqual(distance, 64)
        self.assertEqual(depth, 2)

    def test_short_negative_large_min_dist(self):
        path_a = Path([Segment(2, 'a'), Segment(7, '')])
        path_b = Path([Segment(2, 'b'), Segment(2, '')])

        distance, depth = path_a.dist(path_b, tree_width=64, min_dist=128)

        self.assertEqual(distance, 4096)
        self.assertEqual(depth, 3)

    def test_long_negative(self):
        path_a = Path([Segment(2, 'a'), Segment(7, ''), Segment(5, '')])
        path_b = Path([Segment(2, 'b'), Segment(2, ''), Segment(2, '')])

        distance, depth = path_a.dist(path_b, tree_width=16, min_dist=2)

        self.assertEqual(distance, 16)
        self.assertEqual(depth, 3)

if __name__ == '__main__':
    unittest.main()
