'''
Defines the LogootPathBuilder class, which generates new paths for a logoot list
'''

import random

from visplay_matrix.logoot.structs import Segment, Path

class LogootPathBuilder:
    @staticmethod
    def create_new_path(path_a, path_b, distance, depth, site, tree_width):
        '''
        Returns a new path, which is path_a adjusted by distance, at the given depth
        The distance should place the new path between path_a and path_b

        path_a: Path instance from which to add the distance
        path_b: Path instance, upper bound for the resulting path (site may be pulled from its segments)
        distance: positive integer, the distance from which the resulting path should be
        depth: intger, the depth at which to perform the adjustment and truncate the new path
        site: string, unique identifier for the user creating this path
        tree_width: integer, maximum number of positions that can be in each layer

        '''
        segments = []

        for index in reversed(range(0, depth + 1)):
            pos_a, site_a = path_a.get_segment(index).as_tuple()
            pos_b, site_b = path_b.get_segment(index).as_tuple()

            # Carry any extra distance to the next layer
            new_pos = (pos_a + distance) % tree_width
            distance = (pos_a + distance) // tree_width 

            # For the final segment of the path, always use the site, so paths are unique
            # (Combined with clk value, guarunteed uniqueness of paths)
            # Prefer site_a, then site_b, as long as those sites are not None
            # (which means they are padded segments)
            if index == depth:
                new_site = site

            elif new_pos == pos_a and site_a is not None:
                new_site = site_a

            elif new_pos == pos_b and site_b is not None:
                new_site = site_b

            else:
                new_site = site

            # Construct and push the new segment
            new_segment = Segment(new_pos, new_site)

            segments.insert(0, new_segment)

        return Path(segments)

    @classmethod
    def alloc_paths(cls, path_a, path_b, num_paths, site, tree_width):
        '''
        Returns a list of length num_paths of Path instances that fall between path_a and path_b

        path_a: Path instance, comes before path_b
        path_b: Path instance, comes after path_a
        num_paths: integer, number of paths to determine between the provided paths
        site: string, unique identifier for the replica allocating these paths
        tree_width: integer, maximum number of positions in each layer
        '''

        paths = []
        base_dist = 0

        total_distance, depth = path_a.dist(path_b, tree_width, num_paths + 1)
        step = total_distance // num_paths

        for _ in range(num_paths):
            new_path = cls.create_new_path(path_a, path_b, 
                    base_dist + random.randint(1, step - 1), depth, site, tree_width)

            paths.append(new_path)

            base_dist += step

        return paths

