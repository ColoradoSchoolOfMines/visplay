'''
Implements classes that can accepts stream of LogootList events, and apply
them, while handling duplicated or out-of-order events
'''

from visplay_matrix.logoot.logoot_list import LogootList

class InsertEvent:
    def __init__(self, path, clk, value):
        self.path = path
        self.clk = clk
        self.value = value

    def as_tuple(self):
        return (self.path, self.clk, self.value)

class RemoveEvent:
    def __init__(self, path, clk):
        self.path = path
        self.clk = clk

    def as_tuple(self):
        return (self.path, self.clk)

class LogootListStreamer:
    '''
    Wrapper for the LogootList class that can apply a stream of events, even if
    the events are out of order or include duplicates
    '''

    def __init__(self, site, tree_width):
        self.logoot_list = LogootList(site, tree_width)
        self.tombstones = {} # Necessary to drop earlier clk values from paths since removed
        # (e.g. I2, R1, R2, I1) would insert the path at clk 1 unless we tracked this

    def as_list(self):
        return self.logoot_list.as_list()

    def at_index(self, index):
        return self.logoot_list.at_index(index)

    def insert(self, index, value):
        return self.logoot_list.insert(index, value)

    def remove(self, index):
        return self.logoot_list.remove(index)

    def apply_stream(self, stream):
        for event in stream:
            self._apply_event(event)

    def _apply_event(self, event):
        # Polymorphism seems better, but the logic should be tied to this class...
        if type(event) == InsertEvent:
            self._apply_insert(event)

        elif type(event) == RemoveEvent:
            self._apply_remove(event)

        else:
            raise Exception(f'Invalid event of type {type(event)} in the event stream')

    def _apply_insert(self, event):
        # Discard event if the path at this clk value was already removed (or was duplicated)
        if event.path in self.tombstones and event.clk <= self.tombstones[event.path]:
            return

        node = self.logoot_list.get_by_path(event.path)

        # We insert if the path doesn't exist yet, or the clk value is higher (event is newer)
        if node is None or node.clk is None or event.clk > node.clk:
            self.logoot_list.apply_insert(*event.as_tuple())

            # For optimization purposes, remove the path from the tombstone list if it is there
            if event.path in self.tombstones:
                self.tombstones.pop(event.path)

        # We discard all events this clks less than or equal to the node's clk,
        # because these are either repeats, or old events that we don't want to
        # apply

    def _apply_remove(self, event):
        # Discard event if the path at this clk was already removed
        if event.path in self.tombstones and event.clk <= self.tombstones[event.path]:
            return

        node = self.logoot_list.get_by_path(event.path)

        # We haven't received this event yet, but mark it as removed (no apply_remove necessary)
        if node is None or node.clk is None: 
            self.tombstones[event.path] = event.clk

        # If the clk values match remove the value
        elif event.clk >= node.clk:
            self.logoot_list.apply_remove(*event.as_tuple())

            self.tombstones[event.path] = event.clk

        # We discard all events with clks less than the node's clk, because
        # those are old events that we don't want to apply
