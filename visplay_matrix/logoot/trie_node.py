'''
This file defines the TrieNode classes, which is the building block of the Trie class
'''

from visplay_matrix.logoot.structs import Path

class TrieNode:
    '''
    Represents a node in a sorted Trie. May implement additional functions to
    support O(logn) indexing and ranking

    Tries can access any element by its path in O(log n) time
    Current implementation actually does it in O(k log n) time, where k is num child nodes
    '''

    # TODO: use sortedcontainer for better dictionary performance
    # http://www.grantjenks.com/docs/sortedcontainers/
    def __init__(self, parent=None, segment=None):
        self.parent = parent # Pointer to the parent node
        self.segment = segment # The segment at the tree of the path represented by this node
        self.child_segments = {} # A dictionary of (Segment, TrieNode) pairs
        self.size = 0 # Number of values stored in the tree in and below this node
        self.value = None # stores the value that correspondgs to this node in the trie
        self.clk = None

    def as_list(self):
        '''
        Returns a complete representation of the order of the values stored in the Trie
        '''

        # By default, the list is empty
        output = []

        # The value in the root always comes first
        # Don't insert a None value (represent no value)
        if self.value is not None:
            output.append(self.value)

        # Recursively append the list representation of each child
        for segment, node in sorted(self.child_segments.items()):
            output += node.as_list()

        return output

    def at_index(self, index):
        '''
        Returns the node at the provided index in this substree
        '''

        # TODO: should we check this here, or only in the root?
        if index >= self.size or index < 0:
            raise Exception(f'Index {index} out of bounds for subtree of size {self.size}')

        if index == 0 and self.value is not None:
            return self

        base_index = 0 if self.value is None else 1

        for segment, node in sorted(self.child_segments.items()):
            if base_index <= index < base_index + node.size:
                return node.at_index(index - base_index)

            base_index += node.size

        raise Exception('An error occured while trying to index into the Trie')

    def insert(self, value, clk):
        '''
        Inserts the provided value and clk into this node
        '''

        if self.value is None and self.clk is None:
            self.increment_size()

        self.value = value
        self.clk = clk

    def remove(self):
        '''
        Removes the value(s) stored in this node, and removes the node if possible
        '''

        self.clk = None
        self.value = None

        self.decrement_size()
        
        # Request that this node be removed if it is no longer needed
        if self.parent is not None and len(self.child_segments) == 0:
            self.parent.cleanup_node(self.segment)

    def get_by_path(self, path):
        '''
        Returns the node at the provided path relative to this node, or None if the path
        does not exist
        
        Note: path is destroyed by this function
        '''

        if len(path) == 0:
            return self

        else:
            segment = path.pop()

            if segment not in self.child_segments:
                return None

            return self.child_segments[segment].get_by_path(path)

    def create_path(self, path):
        '''
        Creates a node at the provided path relative to this node, and returns the created node
        Note that if this path already exists, it will simply return that node

        Note: path is destroyed by this function
        '''

        if len(path) == 0:
            return self

        else:
            segment = path.pop()

            if segment not in self.child_segments:
                new_node = TrieNode(self, segment)

                self.child_segments[segment] = new_node

            return self.child_segments[segment].create_path(path)

    def decrement_size(self):
        '''
        Recursively decrements the size of the node, and all it's parents. Called after a
        succesful removal of a path from the Trie
        '''

        self.size -= 1

        if self.parent is not None:
            self.parent.decrement_size()

    def increment_size(self):
        '''
        Recursively increments the size of the node and all its parents
        Called when a node has values assigned to it
        '''

        self.size += 1

        if self.parent is not None:
            self.parent.increment_size()

    def cleanup_node(self, segment):
        '''
        Recursively removes nodes that are no longer needed in the Trie. Called after a
        removal occurs, by a child, on it's parent.

        segment: Segment instance where the child to clean up is in the child_segment dict
        '''

        # Remove the child from the dictionary
        self.child_segments.pop(segment)

        # Have this child removed as well if it is no longer needed (and not a root node)
        if self.value is None and len(self.child_segments) == 0 and self.parent is not None:
            self.parent.cleanup_node(self.segment)

    def get_abs_path(self):
        '''
        Returns a Path instance, the absolute path to this node in the Trie
        '''

        # Base case: a root node has an empty path
        if self.parent is None:
            return Path([])

        # Recursive case: append this node's segment to the end of the parent's abs path
        parent_path = self.parent.get_abs_path()

        parent_path.append(self.segment)

        return parent_path
