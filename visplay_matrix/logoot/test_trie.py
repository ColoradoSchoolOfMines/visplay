'''
Tests the Trie class, a wrapper class for TrieNode, which implements some
high-level functionality. Note that some methods are implicitly tested in the
TrieNode test file, and they are not duplicated here
'''

import unittest

from visplay_matrix.logoot.trie import Trie
from visplay_matrix.logoot.structs import Path

class TestRemove(unittest.TestCase):
    '''
    Verifies that the remove method correctly removes values from the trie
    '''

    def test_empty_list_shallow(self):
        trie = Trie()

        trie.insert(Path([(5, 'a')]), 1, 'test')
        trie.remove(Path([(5, 'a')]))

        self.assertEqual(trie.as_list(), [])

    def test_empty_list_deep(self):
        trie = Trie()

        trie.insert(Path([(5, 'a'), (2, 'b'), (7, 'c')]), 1, 'test')
        trie.remove(Path([(5, 'a'), (2, 'b'), (7, 'c')]))

        self.assertEqual(trie.as_list(), [])

    def test_remove_mid_element(self):
        trie = Trie()

        trie.insert(Path([(5, 'b'), (2, 'b')]), 1, 'a')
        trie.insert(Path([(6, 'a')]), 2, 'b')
        trie.insert(Path([(6, 'a'), (2, 'b'), (2, 'a')]), 3, 'c')
        trie.insert(Path([(6, 'a'), (3, 'c')]), 4, 'd')
        trie.insert(Path([(6, 'b')]), 5, 'e')

        trie.remove(Path([(6, 'a'), (2, 'b'), (2, 'a')]))

        self.assertEqual(trie.as_list(), ['a', 'b', 'd', 'e'])

    def test_remove_first_element(self):
        trie = Trie()

        trie.insert(Path([(5, 'b'), (2, 'b')]), 1, 'a')
        trie.insert(Path([(6, 'a')]), 2, 'b')
        trie.insert(Path([(6, 'a'), (2, 'b'), (2, 'a')]), 3, 'c')
        trie.insert(Path([(6, 'a'), (3, 'c')]), 4, 'd')
        trie.insert(Path([(6, 'b')]), 5, 'e')

        trie.remove(Path([(5, 'b'), (2, 'b')]))

        self.assertEqual(trie.as_list(), ['b', 'c', 'd', 'e'])

    def test_remove_last_element(self):
        trie = Trie()

        trie.insert(Path([(5, 'b'), (2, 'b')]), 1, 'a')
        trie.insert(Path([(6, 'a')]), 2, 'b')
        trie.insert(Path([(6, 'a'), (2, 'b'), (2, 'a')]), 3, 'c')
        trie.insert(Path([(6, 'a'), (3, 'c')]), 4, 'd')
        trie.insert(Path([(6, 'b')]), 5, 'e')

        trie.remove(Path([(6, 'b')]))

        self.assertEqual(trie.as_list(), ['a', 'b', 'c', 'd'])

    def test_remove_multiple(self):
        trie = Trie()

        trie.insert(Path([(5, 'b'), (2, 'b')]), 1, 'a')
        trie.insert(Path([(6, 'a')]), 2, 'b')
        trie.insert(Path([(6, 'a'), (2, 'b'), (2, 'a')]), 3, 'c')
        trie.insert(Path([(6, 'a'), (3, 'c')]), 4, 'd')
        trie.insert(Path([(6, 'b')]), 5, 'e')

        trie.remove(Path([(6, 'a')]))
        trie.remove(Path([(6, 'a'), (3, 'c')]))

        self.assertEqual(trie.as_list(), ['a', 'c', 'e'])

    def test_remove_nonexistent_path(self):
        trie = Trie()

        trie.insert(Path([(5, 'a')]), 1, 'test')

        self.assertRaises(Exception, trie.remove, Path([(6, 'b')]))

if __name__ == '__main__':
    unittest.main()
