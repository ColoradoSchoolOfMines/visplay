'''
Tests the Segment class methods
'''

import unittest

from visplay_matrix.logoot.structs import Segment

class TestComparison(unittest.TestCase):
    '''
    Tests that the comparision operator overloads work correctly
    '''

    def test_same_segment(self):
        segment_a = Segment(0, 'a')
        segment_b = Segment(0, 'a')

        self.assertFalse(segment_a < segment_b)
        self.assertFalse(segment_b < segment_a)
        self.assertEqual(segment_a, segment_b)

    def test_same_pos_diff_site(self):
        segment_a = Segment(5, 'a')
        segment_b = Segment(5, 'b')

        self.assertTrue(segment_a < segment_b)
        self.assertFalse(segment_b < segment_a)
        self.assertNotEqual(segment_a, segment_b)

    def test_diff_pos_same_site(self):
        segment_a = Segment(12, 'a')
        segment_b = Segment(42, 'a')

        self.assertTrue(segment_a < segment_b)
        self.assertFalse(segment_b < segment_a)
        self.assertNotEqual(segment_a, segment_b)

    def test_diff_pos_diff_site(self):
        segment_a = Segment(6, 'a')
        segment_b = Segment(15, 'b')

        self.assertTrue(segment_a < segment_b)
        self.assertFalse(segment_b < segment_a)
        self.assertNotEqual(segment_a, segment_b)

class TestDistance(unittest.TestCase):
    '''
    Tests that the difference between two segments is computed correctly
    There are a lot of tests here, mostly to define the expected behavoir
    '''

    def test_same_segment(self):
        segment_a = Segment(0, 'a')
        segment_b = Segment(0, 'a')

        distance = segment_a.dist(segment_b)

        self.assertEqual(distance, 0)

    def test_same_pos_diff_site(self):
        segment_a = Segment(5, 'a')
        segment_b = Segment(5, 'b')

        distance = segment_a.dist(segment_b)

        self.assertEqual(distance, 0)

    def test_diff_pos_same_site(self):
        segment_a = Segment(12, 'a')
        segment_b = Segment(42, 'a')

        distance = segment_a.dist(segment_b)

        self.assertEqual(distance, 30)

    def test_diff_pos_diff_site(self):
        segment_a = Segment(6, 'a')
        segment_b = Segment(15, 'b')

        distance = segment_a.dist(segment_b)

        self.assertEqual(distance, 9)

    def test_negative_dist(self):
        segment_a = Segment(19, 'a')
        segment_b = Segment(3, 'b')

        distance = segment_a.dist(segment_b)

        self.assertEqual(distance, -16)

if __name__ == '__main__':
    unittest.main()
