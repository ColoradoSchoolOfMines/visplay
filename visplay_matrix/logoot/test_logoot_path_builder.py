import unittest

from unittest.mock import patch
from visplay_matrix.logoot.structs import Path, Segment
from visplay_matrix.logoot.logoot_list import LogootPathBuilder

class TestNormalCreateNewPath(unittest.TestCase):
    '''
    Tests that a path can be adjusted by a provided distance correctly, when the order
    of the paths can be determined by the positions alone. Uses the paths in the tests above
    '''

    def assertPathsMatch(self, path_a, path_b):
        self.assertEqual(len(path_a), len(path_b))

        for i in range(len(path_a)):
            pos_a, site_a = path_a.get_segment(i).as_tuple()
            pos_b, site_b = path_b.get_segment(i).as_tuple()

            self.assertEqual(pos_a, pos_b)
            self.assertEqual(site_a, site_b)

    def test_shallow_gap(self):
        path_a = Path([Segment(0, 'a')])
        path_b = Path([Segment(32, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=22, depth=0, site='c', tree_width=64)

        expected_path = Path([Segment(22, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_shallow_no_gap(self):
        path_a = Path([Segment(16, 'a')])
        path_b = Path([Segment(17, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=8, depth=1, site='c', tree_width=32)

        expected_path = Path([Segment(16, 'a'), Segment(8, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_deep_gap(self):
        path_a = Path([Segment(25, 'a'), Segment(16, 'a'), Segment(0, 'a')])
        path_b = Path([Segment(25, 'b'), Segment(16, 'b'), Segment(64, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=52, depth=2, site='c', tree_width=128)

        expected_path = Path([Segment(25, 'a'), Segment(16, 'a'), Segment(52, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_deep_no_gap(self):
        path_a = Path([Segment(25, 'a'), Segment(16, 'a'), Segment(9, 'a')])
        path_b = Path([Segment(25, 'b'), Segment(16, 'b'), Segment(10, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=17, depth=3, site='c', tree_width=128)

        expected_path = Path([Segment(25, 'a'), Segment(16, 'a'), Segment(9, 'a'), 
            Segment(17, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_multi_level_short_dist(self):
        path_a = Path([Segment(9, 'a'), Segment(10, 'a')])
        path_b = Path([Segment(10, 'b'), Segment(22, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=14, depth=1, site='c', tree_width=32)

        expected_path = Path([Segment(9, 'a'), Segment(24, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_multi_level_long_dist(self):
        path_a = Path([Segment(9, 'a'), Segment(10, 'a')])
        path_b = Path([Segment(10, 'b'), Segment(22, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=38, depth=1, site='c', tree_width=32)

        expected_path = Path([Segment(10, 'b'), Segment(16, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_multi_level_negative(self):
        path_a = Path([Segment(9, 'a'), Segment(22, 'a')])
        path_b = Path([Segment(10, 'b'), Segment(10, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=12, depth=1, site='c', tree_width=32)

        expected_path = Path([Segment(10, 'b'), Segment(2, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_short_first_path_short_dist(self):
        path_a = Path([Segment(9, 'a')])
        path_b = Path([Segment(10, 'b'), Segment(37, 'b'), Segment(22, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=42, depth=1, site='c', tree_width=64)

        expected_path = Path([Segment(9, 'a'), Segment(42, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_short_first_path_long_dist(self):
        path_a = Path([Segment(9, 'a')])
        path_b = Path([Segment(10, 'b'), Segment(37, 'b'), Segment(22, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=75, depth=1, site='c', tree_width=64)

        expected_path = Path([Segment(10, 'b'), Segment(11, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_short_first_path_match_site(self):
        path_a = Path([Segment(9, 'a')])
        path_b = Path([Segment(10, 'b'), Segment(37, 'b'), Segment(22, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=2370, depth=2, site='c', tree_width=64)

        expected_path = Path([Segment(9, 'a'), Segment(37, 'b'), Segment(2, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_long_first_path_shallow(self):
        path_a = Path([Segment(9, 'a'), Segment(37, 'a'), Segment(22, 'a')])
        path_b = Path([Segment(10, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=9, depth=1, site='c', tree_width=64)

        expected_path = Path([Segment(9, 'a'), Segment(46, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_long_first_path_deep(self):
        path_a = Path([Segment(9, 'a'), Segment(37, 'a'), Segment(22, 'a')])
        path_b = Path([Segment(10, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=433, depth=2, site='c', tree_width=64)

        expected_path = Path([Segment(9, 'a'), Segment(44, 'c'), Segment(7, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_distant_paths(self):
        path_a = Path([Segment(2, 'a'), Segment(8, 'a'), Segment(10, 'a')])
        path_b = Path([Segment(7, 'b'), Segment(5, 'b'), Segment(11, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=465, depth=2, site='c', tree_width=16)

        expected_path = Path([Segment(4, 'c'), Segment(5, 'b'), Segment(11, 'c')])

        self.assertPathsMatch(new_path, expected_path)

class TestSpecialCreateNewPath(unittest.TestCase):
    '''
    Tests that a path can be adjusted by a provided distance correctly, when
    the order of the paths must be determined, in part, by using the sites.
    Uses the paths in the tests above
    '''

    def assertPathsMatch(self, path_a, path_b):
        self.assertEqual(len(path_a), len(path_b))

        for i in range(len(path_a)):
            pos_a, site_a = path_a.get_segment(i).as_tuple()
            pos_b, site_b = path_b.get_segment(i).as_tuple()

            self.assertEqual(pos_a, pos_b)
            self.assertEqual(site_a, site_b)

    def test_shallow_identical(self):
        path_a = Path([Segment(2, 'a')])
        path_b = Path([Segment(2, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=11, depth=1, site='c', tree_width=16)

        expected_path = Path([Segment(2, 'a'), Segment(11, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_deep_identical(self):
        path_a = Path([Segment(2, 'a'), Segment(5, 'a')])
        path_b = Path([Segment(2, 'b'), Segment(5, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=7, depth=2, site='c', tree_width=16)

        expected_path = Path([Segment(2, 'a'), Segment(5, 'a'), Segment(7, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_short_negative(self):
        path_a = Path([Segment(2, 'a'), Segment(7, 'a')])
        path_b = Path([Segment(2, 'b'), Segment(2, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=16, depth=2, site='c', tree_width=32)

        expected_path = Path([Segment(2, 'a'), Segment(7, 'a'), Segment(16, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_short_negative_large_min_dist(self):
        path_a = Path([Segment(2, 'a'), Segment(7, 'a')])
        path_b = Path([Segment(2, 'b'), Segment(2, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=120, depth=3, site='c', tree_width=16)

        expected_path = Path([Segment(2, 'a'), Segment(7, 'a'), Segment(7, 'c'), 
            Segment(8, 'c')])

        self.assertPathsMatch(new_path, expected_path)

    def test_long_negative(self):
        path_a = Path([Segment(2, 'a'), Segment(7, 'a'), Segment(5, 'a')])
        path_b = Path([Segment(2, 'b'), Segment(2, 'b'), Segment(2, 'b')])

        new_path = LogootPathBuilder.create_new_path(
                path_a, path_b, distance=7, depth=3, site='c', tree_width=16)

        expected_path = Path([Segment(2, 'a'), Segment(7, 'a'), Segment(5, 'a'), 
            Segment(7, 'c')])

        self.assertPathsMatch(new_path, expected_path)

def mock_randint(start, stop):
    '''
    mock from the random.randint function, that always returns the number in
    the middle of the range
    '''

    return (start + stop + 1) // 2

class TestAllocatePaths(unittest.TestCase):
    '''
    Tests that the functions to allocate new paths work correctly
    '''

    def assertPathsMatch(self, path_a, path_b):
        self.assertEqual(len(path_a), len(path_b))

        for i in range(len(path_a)):
            pos_a, site_a = path_a.get_segment(i).as_tuple()
            pos_b, site_b = path_b.get_segment(i).as_tuple()

            self.assertEqual(pos_a, pos_b)
            self.assertEqual(site_a, site_b)

    def assertPathListsMatch(self, list_a, list_b):
        self.assertEqual(len(list_a), len(list_b))

        for i in range(len(list_a)):
            self.assertPathsMatch(list_a[i], list_b[i])

    @patch('random.randint', mock_randint)
    def test_multiple(self):
        path_a = Path([Segment(9, 'a'), Segment(10, 'a')])
        path_b = Path([Segment(10, 'b'), Segment(22, 'b')])

        paths = LogootPathBuilder.alloc_paths(
                path_a, path_b, num_paths=8, site='c', tree_width=32)

        expected_paths = [
            Path([Segment(9, 'a'), Segment(13, 'c')]),
            Path([Segment(9, 'a'), Segment(18, 'c')]),
            Path([Segment(9, 'a'), Segment(23, 'c')]),
            Path([Segment(9, 'a'), Segment(28, 'c')]),
            Path([Segment(10, 'b'), Segment(1, 'c')]),
            Path([Segment(10, 'b'), Segment(6, 'c')]),
            Path([Segment(10, 'b'), Segment(11, 'c')]),
            Path([Segment(10, 'b'), Segment(16, 'c')]),
        ]

        self.assertPathListsMatch(paths, expected_paths)

    @patch('random.randint', mock_randint)
    def test_single(self):
        path_a = Path([Segment(9, 'a'), Segment(10, 'a')])
        path_b = Path([Segment(10, 'b'), Segment(22, 'b')])

        path = LogootPathBuilder.alloc_paths(path_a, path_b, num_paths = 1, 
                site='c', tree_width=32)[0]

        expected_path = Path([Segment(10, 'b'), Segment(0, 'c')])

        self.assertPathsMatch(path, expected_path)


if __name__ == '__main__':
    unittest.main()
