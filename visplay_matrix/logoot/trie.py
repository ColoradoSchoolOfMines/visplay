'''
This file defines the Trie, the data structure that stores all the data in a LogootList
'''

from copy import deepcopy

from visplay_matrix.logoot.trie_node import TrieNode

class Trie:
    '''
    Represents a complete Trie. Acts as a wrapper to the root node of a TrieNode, and 
    provides additional functionality that can be guarunteed by a root node 
    '''

    def __init__(self):
        self.root = TrieNode()

    def as_list(self):
        return self.root.as_list()

    def at_index(self, index):
        return self.root.at_index(index)

    def get_by_path(self, path):
        return self.root.get_by_path(deepcopy(path))

    # TODO: prevent insertion of an empty path?
    def insert(self, path, clk, value):
        '''
        Inserts the provided value at the provided path in the Trie, along with
        the given clk value for the insertion

        path: Path instance, defines the order to insert the value at
        clk: logical clock value to represent when the element was inserted
        value: the value to insert at the end of the path
        '''

        # Copy the path since this method destroys the path
        node = self.root.create_path(deepcopy(path))

        node.insert(value, clk)

    def remove(self, path):
        '''
        Removes the provided path from the Trie (or raises an Exception if that
        path is not in the Trie
        '''

        # Copy the path since this method destroys the path
        node = self.root.get_by_path(deepcopy(path))

        if node is None:
            raise Exception('The provided path does not exist in the trie')

        node.remove()
