#!/usr/bin/env bash

set -xe

# Render the graphics
pushd docs/graphics
make
popd

# Compile the Sphinx documenation
make html

# Required for GitLab pages
mv build/html public
