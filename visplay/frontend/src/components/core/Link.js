import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * Link component for conviniently generating an anchor tag.
 */
class Link extends Component {
  static propTypes = {
    href: PropTypes.string.isRequired,
    linkText: PropTypes.string,
    title: PropTypes.string,
  };

  render() {
    const title = this.props.title || '';
    const linkText = this.props.linkText || this.props.href;
    return <a href={this.props.href} title={title}>{linkText}</a>;
  }
}

export default Link;

